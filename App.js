import { NavigationContainer } from '@react-navigation/native';
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Login from './screens/login';
import Home from './screens/home';
import Contact from './screens/contact';
import Splash from './screens/splash'
import Header from './components/header';

const Stack = createNativeStackNavigator();

export default function App() {

  const removeItem = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
      console.log(`Item with key '${key}' has been removed from AsyncStorage.`);
    } catch (error) {
      console.error("Error removing item from AsyncStorage:", error);
    }
  };

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={({ route, navigation }) => ({
              header: (props) => (
                <Header
                  {...props}
                  title={route.name}
                  onPressLogout={async () => {
                    console.log("Logout button pressed");
                    removeItem("token");
                    removeItem("contact");
                    const token = await AsyncStorage.getItem("token");
                    if (!token) {
                      navigation.navigate("Login");
                    }
                  }}
                />
              ),
            })}
          />
          <Stack.Screen
            name="Contact"
            component={Contact}
            options={({ route, navigation }) => ({
              header: (props) => (
                <Header
                  {...props}
                  title={route.name}
                  showBackButton={navigation.canGoBack()}
                  onPressLogout={async () => {
                    console.log("Logout button pressed");
                    removeItem("token");
                    removeItem("contact");
                    const token = await AsyncStorage.getItem("token");
                    if (!token) {
                      navigation.navigate("Login");
                    }
                  }}
                />
              ),
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </GestureHandlerRootView>
  );
}


