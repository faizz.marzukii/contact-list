import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Contact({ route }) {
  const { user } = route.params;
  const navigation = useNavigation(); // Hook to access navigation object

  const removeUser = async () => {
    try {
      // Fetch the existing contacts from AsyncStorage
      const existingContacts = await AsyncStorage.getItem("contact");
      console.log("Existing Contacts:", existingContacts);

      let contacts = existingContacts ? JSON.parse(existingContacts) : [];

      // Remove the user based on login.uuid
      contacts = contacts.filter(
        (contact) => contact.login.uuid !== user.login.uuid
      );

      // Save the updated list of contacts to AsyncStorage
      await AsyncStorage.setItem("contact", JSON.stringify(contacts));
      console.log("Updated Contacts:", JSON.stringify(contacts));
      console.log("Success: User removed");

      // Navigate back to the home screen after removing the user
      navigation.navigate("Home");
    } catch (error) {
      console.error("Error removing user:", error);
    }
  };


  const handleUnfriend = () => {
    removeUser(); // Call the function to remove the user
    // No need for additional navigation here as it's handled in removeUser()
  };

  return (
    <View className="flex flex-col items-center justify-center mt-5">
      {/* Centered content */}
      <View className="items-center">
        <View className="my-4">
          <Image
            className="w-24 h-24 rounded-full"
            source={{ uri: user.picture.large }}
          />
        </View>
        <Text className="text-center font-bold text-xl">
          {`${user.name.title} ${user.name.first} ${user.name.last}`}
        </Text>
        <Text className="text-center font-medium text-base text-gray-500">
          {user.login.username}
        </Text>
        <Text className="text-center">{user.email}</Text>
      </View>

      {/* Details on the left */}
      <View className="my-8 ml-4 self-start">
        <Text className="text-lg font-bold">Details</Text>
        <Text className="font-medium text-base text-black">Phone Number :{user.phone}</Text>
        {/* Add more details here */}
      </View>

      {/* Unfriend button on the left */}
      <View className="self-center mx-4 w-96">
        <TouchableOpacity className="bg-red-600 py-2 px-4 rounded-lg" onPress={handleUnfriend}>
          <Text className="text-white text-center">Unfriend</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
