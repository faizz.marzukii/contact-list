import React, { useEffect } from 'react';
import {View, Text} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";

const Splash = ({navigation}) => {

    useEffect(() => {
      const checkToken = async () => {
        try {
          const token = await AsyncStorage.getItem("token");
          setTimeout(() => {
            if (token) {
              // If token exists, navigate to Home
              navigation.replace("Home");
            } else {
              // If token doesn't exist, navigate to Login
              navigation.replace("Login");
            }
          }, 2000);
        } catch (error) {
          console.log(error);
          // Handle error, maybe navigate to Login as a fallback
          navigation.replace("Login");
        }
      };

      // Check token on component mount
      checkToken();
    }, [navigation]);

    return (
      <View className="flex flex-1 bg-blue-600 items-center justify-center">
        <Text className=" text-lg font-extrabold text-gray-100">ContactList</Text>
      </View>
    );
}

export default Splash;
