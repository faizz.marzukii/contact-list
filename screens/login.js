import { View, Text, TextInput, Button, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import CryptoJS from "crypto-js";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Login({ navigation }) {
  const [profile, setProfile] = useState([]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const jwt =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImJlYXV0aWZ1bHRpZ2VyMjk1IiwicGFzc3dvcmQiOiJzZWxlbmEifQ.MTgOGjH8ci69hC8O5a2O7ZgYyJJZq5meJLTHhk4AzQg";

  const url = "https://randomuser.me/api/?seed=lll";
  const hashPassword = CryptoJS.SHA256(profile.password).toString(
    CryptoJS.enc.Hex
  );
  const userHashPassword = CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);

  const setToken = async (token) => {
    try {
      await AsyncStorage.setItem("token", token);
    } catch (error) {
      console.log("error");
    }
  };

  const handleLogin = () => {
    if (username === profile.username && hashPassword === userHashPassword) {
      setToken(jwt);
      console.log("Success");
      navigation.replace("Home");
    } else {
      console.log("error");
    }
  };

  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.get(url);
        if (response) {
          const data = response.data.results[0].login;
          setProfile(data);
        }
      } catch (error) {}
    }
    fetchData();
  }, [profile]);

  return (
    <View className="flex-1 bg-white flex items-center justify-center">
      <Text>Login</Text>
      <View className="flex items-center gap-3">
        <View>
          <Text>Username</Text>
          <View className="flex-row items-center border border-gray-400 rounded-md p-2 w-1/2">
            <TextInput
              placeholder="Enter your username"
              onChangeText={(text) => setUsername(text)}
              value={username}
              style={{ flex: 1 }}
            />
          </View>
        </View>
        <View>
          <Text>Password</Text>
          <View className="flex-row items-center border border-gray-400 rounded-md p-2 w-1/2">
            <TextInput
              placeholder="Enter your password"
              onChangeText={(text) => setPassword(text)}
              value={password}
              secureTextEntry={!showPassword}
              style={{ flex: 1 }}
            />
            <TouchableOpacity onPress={toggleShowPassword}>
              <Ionicons
                name={showPassword ? "eye-off" : "eye"}
                size={24}
                color="black"
                style={{ marginLeft: 5 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <Button title="Login" onPress={handleLogin}>
            Log In
          </Button>
        </View>
      </View>
    </View>
  );
}
