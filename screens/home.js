import { View, FlatList, ActivityIndicator } from "react-native";
import React, { useEffect, useState, useCallback } from "react";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useFocusEffect } from "@react-navigation/native";
import Card from "../components/card";

export default function Home() {
  const [contactList, setContactList] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const maxContacts = 100; // Maximum number of contacts to fetch

  const fetchContact = async (pageNumber) => {
    setLoading(true);
    try {
      const response = await axios.get(
        `https://randomuser.me/api/?seed=lll&page=${pageNumber}&results=25`
      );
      const newContacts = response.data.results;
      if (contactList.length < maxContacts) {
        const updatedContacts = contactList.concat(
          newContacts.slice(0, maxContacts - contactList.length)
        );
        setContactList(updatedContacts);
        setPage(pageNumber + 1);
        saveContact(updatedContacts); // Save the updated contacts
      }
    } catch (error) {
      console.error("Error fetching contacts:", error);
    } finally {
      setLoading(false);
    }
  };

  const saveContact = async (array) => {
    try {
      await AsyncStorage.setItem("contact", JSON.stringify(array));
      console.log("Array of objects saved successfully!");
    } catch (error) {
      console.error("Error saving array:", error);
    }
  };

  const loadContactsFromStorage = async () => {
    try {
      const stringifiedArray = await AsyncStorage.getItem("contact");
      if (stringifiedArray !== null) {
        const parsedArray = JSON.parse(stringifiedArray);
        console.log('current userlist:', parsedArray);
        setContactList(parsedArray); // Update state with contacts from AsyncStorage
      } else {
        console.log("No data found");
      }
    } catch (error) {
      console.error("Error retrieving array:", error);
    }
  };

  const fetchContactsIfNeeded = async () => {
    try {
      const stringifiedArray = await AsyncStorage.getItem("contact");
      if (stringifiedArray === null) {
        // If AsyncStorage is empty, fetch contacts
        await fetchContact(page);
      } else {
        // AsyncStorage has data, load it
        loadContactsFromStorage();
      }
    } catch (error) {
      console.error("Error checking AsyncStorage:", error);
    }
  };

  const handleDeleteContact = async (contactToDelete) => {
    try {
      const updatedContacts = contactList.filter(
        (contact) => contact.login.uuid !== contactToDelete.login.uuid
      );

      await AsyncStorage.setItem("contact", JSON.stringify(updatedContacts)); // Update AsyncStorage content

      setContactList(updatedContacts); // Update state with filtered contacts
    } catch (error) {
      console.error("Error deleting contact:", error);
      // Handle errors here
    }
  };

  useEffect(() => {
    fetchContactsIfNeeded(); // Check AsyncStorage on mount
  }, []);

  useFocusEffect(
    useCallback(() => {
      fetchContactsIfNeeded();
    }, [])
  );

  const loadMoreContacts = useCallback(() => {
    if (!loading && contactList.length < maxContacts) {
      fetchContact(page);
    }
  }, [loading, page, contactList]);

  const renderFooter = () => {
    if (!loading || contactList.length >= maxContacts) return null;
    return <ActivityIndicator style={{ marginVertical: 20 }} />;
  };

  // Ensure keys are unique for each item
  const keyExtractor = (item, index) => `${item.id}-${index}`;

  return (
    <View>
      <FlatList
        data={contactList}
        renderItem={({ item }) => (
          <Card contact={item} onDelete={() => handleDeleteContact(item)} />
        )}
        keyExtractor={keyExtractor}
        ItemSeparatorComponent={() => <View style={{ height: 20 }} />}
        onEndReached={loadMoreContacts}
        onEndReachedThreshold={0.8} // Trigger loadMoreContacts when the user is 10% away from the end
        ListFooterComponent={renderFooter}
        // className="my-5 mx-3" // Remove this as className is not a valid prop for FlatList in React Native
      />
    </View>
  );
}
