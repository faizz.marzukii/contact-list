import React from "react";
import {
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const Card = ({ contact, onDelete }) => {
  const navigation = useNavigation();

  const handleCardPress = () => {
    navigation.navigate("Contact", { user: contact });
  };

  return (
    <TouchableWithoutFeedback onPress={handleCardPress}>
      <View className="flex-row items-center p-4 rounded-lg bg-gray-300 my-1 mx-2">
        <View style={{ marginRight: 10 }}>
          <Image
            className="w-12 h-12 rounded-full"
            source={{ uri: contact.picture.thumbnail }}
          />
        </View>
        <View className="flex-1">
          <Text className="text-lg font-bold text-left">
            {contact.name.first} {contact.name.last}
          </Text>
          <Text>{contact.email}</Text>
          <Text className="text-sm">{contact.phone}</Text>
        </View>
        <TouchableOpacity onPress={onDelete}>
          <Ionicons name="trash-bin" size={24} color="red" />
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Card;
