import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const Header = ({ title, showBackButton, onPressLogout }) => {
  const navigation = useNavigation();

  const handleBack = () => {
    navigation.goBack();
  };

  return (
    <View className="flex-row items-center justify-between h-auto px-4 bg-blue-600 pt-10 pb-5">
      {showBackButton && (
        <TouchableOpacity onPress={handleBack} className="pr-2">
          <Ionicons name="arrow-back" size={24} color="white" />
        </TouchableOpacity>
      )}
      <Text className="text-lg font-extrabold text-gray-100">{title}</Text>
      <TouchableOpacity onPress={onPressLogout} className="pl-2">
        <Ionicons name="log-out" size={24} color="white" />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
